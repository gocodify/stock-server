package main

import (
	"flag"
	"log"
)

const (
	worldTradingAPIKey = "4673sK6SL18Vwu26ewEyF07xjlESvhM2q7R9eUXeshtbZ1foWk45ZSv9Eh0G"
	serverError        = "Something went wrong. Please contact your system administrator."
)

func init() {
	log.SetFlags(log.Ldate | log.Lmicroseconds | log.Lshortfile)

	flag.String("port", "", "port to listen on")

	flag.Parse()

	if flag.Lookup("port").Value.String() == "" {
		log.Fatal("-port is required")
	}
}
