package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

func getStockPricesHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	symbol := vars["symbol"]
	if symbol == "" {
		log.Println("Empty sybol received.")
		sendResponse(w, false, "symbol is required.")
		return
	}

	stockExchangeNames := make(map[string]bool, 0)
	stockExchangeQueryValue := r.URL.Query().Get("stock_exchange")

	if stockExchangeQueryValue == "" {
		// AMEX is default stock exchange.
		stockExchangeNames["AMEX"] = true
	} else {
		allStockExchangeReceived := strings.Split(stockExchangeQueryValue, ",")
		for _, v := range allStockExchangeReceived {
			stockExchangeNames[strings.Trim(v, " ")] = true
		}
	}

	req, err := http.NewRequest(http.MethodGet, "https://api.worldtradingdata.com/api/v1/stock", nil)
	if err != nil {
		log.Println(err)
		sendResponse(w, false, serverError)
		return
	}

	queryParams := req.URL.Query()
	queryParams.Add("symbol", symbol)
	queryParams.Add("api_token", worldTradingAPIKey)
	req.URL.RawQuery = queryParams.Encode()

	client := http.Client{}

	res, err := client.Do(req)
	if err != nil {
		log.Println(err)
		sendResponse(w, false, serverError)
		return
	}
	defer res.Body.Close()

	var stockResponse StockResponse
	if err := json.NewDecoder(res.Body).Decode(&stockResponse); err != nil {
		log.Println(err)
		sendResponse(w, false, serverError)
		return
	}

	if stockResponse.Message != "" {
		log.Println(stockResponse.Message)
		sendResponse(w, false, stockResponse.Message)
		return
	}

	// Preparing response data.
	mapOfStockExchangeToStockPrice := make(map[string]StockPrice, 0)
	for _, v := range stockResponse.Data {
		if _, containsKey := stockExchangeNames[v.StockExchangeShort]; containsKey {
			mapOfStockExchangeToStockPrice[v.StockExchangeShort] = v.StockPrice
		}
	}

	sendResponse(w, true, mapOfStockExchangeToStockPrice)
}
