# 1. Instructions to run this project.
Install go as specified on https://golang.org. Then follow the following steps:
- Clone this repository in $HOME/go/src
- cd stock-server

Below are the external go depenencies.
- github.com/gorilla/mux

- This library implements a request router and dispatcher for matching incoming requests to their respective handler.
- mux.Router matches incoming requests against a list of registered routes and calls a handler for the route that matches the URL or other conditions.

To download and install packages use the below commands.
- go get github.com/gorilla/mux

To run this project:
- go install && stock-server -port 12000

# 2. Test Cases.

## a. http://localhost:12000/stock/AAPL?stock_exchange=NASDAQ,NYSE

### Response: 

```json
{
	"NASDAQ": {
    "symbol": "AAPL",
    "name": "Apple Inc.",
    "price": "175.07",
    "close_yesterday": "178.30",
    "currency": "USD",
    "market_cap": "805511102464",
    "volume": "23323842",
    "timezone": "EDT",
    "timezone_name": "America/New_York",
    "gmt_offset": "-14400",
    "last_trade_time": "2019-05-31 16:00:01"
	}
}
```

## b. http://localhost:12000/stock/BCNA?stock_exchange=NASDAQ,NYSE

### Response: 

```json
{
	"NASDAQ": {
		"symbol": "BCNA",
		"name": "Reality Shares Nasdaq NexGen Economy China ETF",
		"price": "20.65",
		"close_yesterday": "20.76",
		"currency": "USD",
		"market_cap": "N/A",
		"volume": "1670",
		"timezone": "EDT",
		"timezone_name": "America/New_York",
		"gmt_offset": "-14400",
		"last_trade_time": "2019-05-31 15:00:38"
	}
}
```

## c. http://localhost:12000/stock/BCNA

### Response: 

```json
{}
```

## d. http://localhost:12000/stock/AAU

### Response: 

```json
{
	"AMEX": {
		"symbol": "AAU",
		"name": "Almaden Minerals Ltd.",
		"price": "0.46",
		"close_yesterday": "0.43",
		"currency": "USD",
		"market_cap": "51714868",
		"volume": "177316",
		"timezone": "EDT",
		"timezone_name": "America/New_York",
		"gmt_offset": "-14400",
		"last_trade_time": "2019-05-31 15:53:11"
	}
}
```

## e. http://localhost:12000/stock/wrong-value?stock_exchange=NASDAQ,NYSE

### Response: 

```json
{
	"ErrorMessage": "Error! The requested stock(s) could not be found."
}
```

## f. http://localhost:12000/stock/ABM?stock_exchange=NASDAQ,NYSE

### Response: 

```json
{
	"NYSE": {
		"symbol": "ABM",
		"name": "ABM Industries Incorporated",
		"price": "36.25",
		"close_yesterday": "36.61",
		"currency": "USD",
		"market_cap": "2401163776",
		"volume": "229042",
		"timezone": "EDT",
		"timezone_name": "America/New_York",
		"gmt_offset": "-14400",
		"last_trade_time": "2019-05-31 16:02:00"
	}
}
```

## g. http://localhost:12000/stock/AAOI,AAN?stock_exchange=NASDAQ,NYSE

### Response: 

```json
{
	"NASDAQ": {
		"symbol": "AAOI",
		"name": "Applied Optoelectronics, Inc.",
		"price": "8.66",
		"close_yesterday": "8.98",
		"currency": "USD",
		"market_cap": "172676928",
		"volume": "355759",
		"timezone": "EDT",
		"timezone_name": "America/New_York",
		"gmt_offset": "-14400",
		"last_trade_time": "2019-05-31 16:00:01"
	},
	"NYSE": {
		"symbol": "AAN",
		"name": "Aaron's, Inc.",
		"price": "53.26",
		"close_yesterday": "54.67",
		"currency": "USD",
		"market_cap": "3604498176",
		"volume": "478658",
		"timezone": "EDT",
		"timezone_name": "America/New_York",
		"gmt_offset": "-14400",
		"last_trade_time": "2019-05-31 16:02:01"
	}
}
```