package main

import (
	"encoding/json"
	"log"
	"net/http"
)

func sendResponse(w http.ResponseWriter, ok bool, value interface{}) {
	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "\t")

	if !ok {
		response := make(map[string]interface{}, 0)
		response["ErrorMessage"] = value

		if err := encoder.Encode(response); err != nil {
			log.Println(err)
		}

		return
	}

	if err := encoder.Encode(value); err != nil {
		log.Println(err)
	}
}
