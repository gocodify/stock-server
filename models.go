package main

// StockPrice ...
type StockPrice struct {
	Symbol         string `json:"symbol"`
	Name           string `json:"name"`
	Price          string `json:"price"`
	CloseYesterday string `json:"close_yesterday"`
	Currency       string `json:"currency"`
	MarketCap      string `json:"market_cap"`
	Volume         string `json:"volume"`
	TimeZone       string `json:"timezone"`
	TimeZoneName   string `json:"timezone_name"`
	GMTOffset      string `json:"gmt_offset"`
	LastTradeTime  string `json:"last_trade_time"`
}

// StockResponse ...
type StockResponse struct {
	SymbolsRequested int    `json:"symbols_requested"`
	SymbolsReturned  int    `json:"symbols_returned"`
	Message          string `json:"Message"`
	Data             []struct {
		StockPrice
		StockExchangeShort string `json:"stock_exchange_short"`
	} `json:"data"`
}
