package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/stock/{symbol}", getStockPricesHandler)

	server := &http.Server{
		Addr:    ":" + flag.Lookup("port").Value.String(),
		Handler: noCacheMW(router),
	}

	flag.VisitAll(func(flag *flag.Flag) {
		log.Println(flag.Name, "->", flag.Value)
	})

	if err := server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}

	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)
	<-stopChan
	log.Println("initiating shutdown...")
	ctx, cancelFunc := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelFunc()
	server.Shutdown(ctx)
	log.Println("graceful shutdown complete.")
}
